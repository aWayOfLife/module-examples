import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SettingsRoutingModule } from './settings-routing.module';
import { SettingsComponent } from './settings.component';
import { AccountSettingsComponent } from './account-settings/account-settings.component';
import { PaymentSettingsComponent } from './payment-settings/payment-settings.component';
import { CoreModule } from 'src/app/core/core.module';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [SettingsComponent, AccountSettingsComponent, PaymentSettingsComponent],
  imports: [
    CommonModule,
    SettingsRoutingModule,
    CoreModule,
    SharedModule
  ]
})
export class SettingsModule { }
