import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AccountSettingsComponent } from './account-settings/account-settings.component';
import { PaymentSettingsComponent } from './payment-settings/payment-settings.component';
import { SettingsComponent } from './settings.component';


const routes: Routes = [
  {
    path: '',
    component: SettingsComponent,
    children: [
      {
        path: '',
        redirectTo: 'account-settings',
        pathMatch: 'full'
      },
      {
        path: 'account-settings',
        component: AccountSettingsComponent
      },
      {
        path: 'payment-settings',
        component: PaymentSettingsComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SettingsRoutingModule { }
