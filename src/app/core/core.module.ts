import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VowelTogglePipe } from './pipes/vowel-toggle.pipe';
import { TextDirective } from './directives/text.directive';



@NgModule({
  declarations: [VowelTogglePipe, TextDirective],
  imports: [
    CommonModule
  ]
})
export class CoreModule { }
